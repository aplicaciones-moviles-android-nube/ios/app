# Utiliza una imagen base con Node.js y npm instalado
FROM node:latest as build
# Establece el directorio de trabajo en el contenedor
WORKDIR /usr/src/app
# Copia los archivos de la aplicación al contenedor
COPY . .
# Instala las dependencias del proyecto
RUN npm install
# Compila la aplicación
RUN npm run build
# Imagen ligera para servir la aplicación
FROM nginx:latest
# Elimina la configuración predeterminada de Nginx
RUN rm -rf /etc/nginx/conf.d/*
# Copia el archivo de configuración personalizado de Nginx que incluye la configuración del proxy inverso
COPY nginx.conf /etc/nginx/conf.d/default.conf
# Copia los archivos de compilación de la aplicación a la carpeta de contenido estático de Nginx
COPY --from=build /usr/src/app/build /usr/share/nginx/html
# Expone el puerto en el que la aplicación se servirá
EXPOSE 8080
# El comando por defecto de la imagen de Nginx ya es adecuado para servir la aplicación React, por lo que no es necesario especificar un CMD
